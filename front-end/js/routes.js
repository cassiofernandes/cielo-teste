angular.module('appRoutes', ['ngRoute'])

.config(function($routeProvider, $locationProvider){
	$routeProvider

	.when("/", {
        templateUrl : "views/home.html",
        controller: "homeController"
    })
    .when("/registros", {
        templateUrl : "views/registros.html",
        controller: "registrosController"
    })
    .when("/apontadores", {
        templateUrl : "views/apontadores.html"
   
    })
    .otherwise({ redirectTo: '/' })

    $locationProvider.html5Mode({
    	enabled: true,
    	requireBase: false
    });
});